class JsTreeExt {
	constructor(treediv_sel, treecontainerdiv_sel, detailsdiv_sel, basketdiv_sel, url_prefix, load_prefix, details_prefix, search_prefix) {
		this.treediv_sel = treediv_sel;
		this.treecontainerdiv_sel = treecontainerdiv_sel;
		this.detailsdiv_sel = detailsdiv_sel;
		this.basketdiv_sel = basketdiv_sel;
		this.url_prefix = url_prefix;
		this.load_prefix = load_prefix;
		this.details_prefix = details_prefix;
		this.search_prefix = search_prefix;
	}
	
	/**
	 * generate the rest url for loading items; needle is the id of the parent element
	 * 
	 * if decendants is true, load all its decendants, not only the first level
	 * if parents is true, also load the parents of needle
	 */
	generate_load_url(needle, decendants, parents) {
		return this.url_prefix + this.load_prefix + needle + "?decendants=" + (decendants ? "true" : "false") + "&parents=" + (parents ? "true" : "false");
	}
	
	/**
	 * generate the rest url for loading details; needle is the id of the element
	 */
	generate_details_url(needle) {
		// TODO: add locale
		return this.url_prefix + this.details_prefix + needle;
	}
	
	/**
	 * generate the search url; needle is the search text
	 */
	generate_search_url(needle) {
		return this.url_prefix + this.search_prefix + needle;
	}

	/**
	 * search for needle, if in_db is true, also use the database to search for it
	 */	
	search(needle, in_db) {
	  if (in_db && needle != "") {
		  console.log("JsTreeExt.search: load from db: " + needle);
			let url = this.generate_load_url(needle, true, true);
		  this.reload_children(needle, false);
		  url = this.generate_search_url(needle);
		  $.ajax({
				method: "GET",
				url: url,
				xhrFields: {
					withCredentials: true
				},
				dataType: "json",
				cache: false,
				success: function(data) {
					for (const entry in data) {
						var value = data[entry];
						// TODO: jsTreeExt ist defined globally in welcome.html; find a better way to get that reference
						jsTreeExt.reload_children(value, false, true);
					}
				},
				error: function(jqXHR, textStatus) {
				  alert(textStatus);
			  }
		  });
	  }
		$(this.treediv_sel).jstree().search(needle);
		// also search for id's
		let node = $(this.treediv_sel).jstree().get_node(needle);
		if (node != undefined & node.text != undefined) {
			console.log("JsTreeExt.search: also search for " + node.text);
			$(this.treediv_sel).jstree().search(node.text, false, false, null, true);
		}
	}
	
	/**
	 * refresh the basked div
	 */
	_refresh_basket(treediv_sel, basketdiv_sel) {
		let vararray = $(treediv_sel).jstree("get_checked");
		if (Array.isArray(vararray)) {
			let container = $("<div>");
			var that = this;
			vararray.forEach(function(value, index, vararray) {
				let name = value.toString();
				// the search plugin only searches for the text, not for the ID, so load the text here
				let node = $(treediv_sel).jstree().get_node(name);
				let text = node.text;
				let the_link = $("<span>" + name + "</span>").css("cursor", "pointer");
				the_link = the_link.attr("title", text);
				the_link = the_link.click(function() {
					// TODO: jsTreeExt ist defined globally in welcome.html; find a better way to get that reference
					jsTreeExt.search(text, false);
				});
				container.append(the_link);
				 // it is important to have space here (for breaks in div),
				 // but it is also necessary to have &nbsp; to separate the items one from another
				container.append("&nbsp; &nbsp;");
			}, that);
				
			$(basketdiv_sel).html(container);
		}
	}
		
	refresh_basket() {
		this._refresh_basket(this.treediv_sel, this.basketdiv_sel);
	}

	/**
   * refresh the details div out of the class scope
   */
  _refresh_details(detailsdiv_sel, unique_name) {
	  progress.start_by_selector(detailsdiv_sel);
	  var url = this.generate_details_url(unique_name);
	  var htmlResult = "<table style=\"width: 100%\">\n<tr><td>unique_name</td><td>" + unique_name + "</td></tr>\n";
	  var the_div_sel = detailsdiv_sel;
		$.ajax({
			method: "GET",
			url: url,
			xhrFields: {
				withCredentials: true
			},
			beforeSend: function(xhr) {
				progress.start_by_selector(the_div_sel);
			},
			dataType: "json",
			cache: false,
			success: function(data) {
				for (const entry in data) {
					var value = data[entry];
					if (value != "") {
						var interpretedValue = jsTreeExt.json2html(value);
						htmlResult = htmlResult + "<tr><td>" + entry + "</td><td>" + interpretedValue + "</td></tr>\n";
					}
				}
			},
			error: function(jqXHR, textStatus) {
			  alert(textStatus);
		  }, 
		  complete: function() {
			  htmlResult = htmlResult + "</table>\n";
				progress.set_in_selector(the_div_sel, htmlResult);
				progress.stop_by_selector(the_div_sel);
			}
		});
	}
	
	/**
   * refresh the details div
   */
	refresh_details() {
		var selected = $(this.treediv_sel).jstree().get_selected(false);
		// always use the first selection if multiple have been made
		var unique_name = selected.length > 0 ? selected[0] : "";
		this._refresh_details(this.detailsdiv_sel, unique_name);
	}

	/**
	 *
	 */
	reload_children(unique_name, decendants, parents) {
		let url = this.generate_load_url(unique_name, decendants, parents);
		var treediv_sel = this.treediv_sel;
		var basketdiv_sel = this.basketdiv_sel;
		var jstree = $(this.treediv_sel).jstree();
		var that_refresh_basket = this._refresh_basket;
		$.ajax({
			method: "GET",
			url: url,
			xhrFields: {
				withCredentials: true
			},
			beforeSend: function(xhr) {
				progress.start();
			},
			dataType: "json",
			cache: false,
			success: function(children) {
				// only 1 child is added at one call of create_node, so loop over array
				children.forEach(function(current, index, arr) {
					// omit adding an element again - would break the component
					if (!jstree.get_node(current.id)) {
						var parent = current;
						if (current.parent != "#") {
							parent = jstree.get_node(current.parent);
						}
						jstree.create_node(parent, current, "last");
					}
				}, this);
			},
			error: function(jqXHR, textStatus) {
			  alert(textStatus);
		  }, 
		  complete: function() {
				if (unique_name != null) {
					var node = jstree.get_node(unique_name);
					// toggle does not work on undefined (not yet loaded) states
					if (jstree.is_open(node)) {
						jstree.close_node(node);
					} else {
						jstree.open_node(node);
					}
				}
				that_refresh_basket(treediv_sel, basketdiv_sel);
				progress.stop();
			}
		});
	}
	
	json2html(jsonString) {
		try {
			var jsonObject = JSON.parse(jsonString);
			if (jsonObject == '"null"') {
				return "";
			} else if (Object.keys(jsonObject).length > 0) {
				var keys = Object.keys(jsonObject);
				var buf = "";
				keys.forEach(function(current){
					buf += "<span class=\"badgeleft\">" + current + "</span>";
					buf += "<span class=\"badgeright\">" + jsTreeExt.json2html(jsonObject[current]) + "</span>";
				});
			  return buf;
			} else if (Array.isArray(jsonObject)) {
				var buf = "<table>";
				for (var i = 0; i < jsonObject.length; i++) {
					buf += "<tr><td>";
					buf += jsTreeExt.json2html(jsonObject[i]);
					buf += "</td></tr>";
				}
				buf += "</table>";
				return buf;
			} else {
				return jsonString; // obviously, a valid json string, but not an object or array
			}
		} catch(e) {
			let splitable = String(jsonString);
			console.log("try to split >" + splitable + "< by |...");
			if (splitable.search("\\|") >= 0) {
				var parts = splitable.split("|");
				if (parts.length > 1) {
					var res = "";
					parts.forEach(function(elem){
						var converted = jsTreeExt.json2html(elem);
						res += "<span class=\"badge\">";
						res += converted;
						res += "</span>";
					})
					return res;
				}
			}
			return jsonString;
		}
	}
}
