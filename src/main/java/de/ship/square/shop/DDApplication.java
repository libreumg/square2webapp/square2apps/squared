package de.ship.square.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author henkej
 *
 */
@SpringBootApplication
public class DDApplication{

	public static void main(String[] args) {
		SpringApplication.run(DDApplication.class, args);
	}

}
