package de.ship.square.shop.common.xlsx;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import de.ship.square.shop.modules.index.impl.missing.MissingBean;

/**
 * 
 * @author henkej
 *
 */
public class ExcelConverter {
	private final static String SHEETNAME = "missinglist";
	private final static String CODE_VALUE = "CODE_VALUE";
	private final static String CODE_LABEL = "CODE_LABEL";
	private final static String CODE_CLASS = "CODE_CLASS";

	private XSSFWorkbook workbook;
	private XSSFSheet sheet;

	private List<MissingBean> list;

	public ExcelConverter(List<MissingBean> missingBeans) {
		this.list = missingBeans;
		workbook = new XSSFWorkbook();
	}

	/**
	 * create a cell of the xlsx file
	 * 
	 * @param row         the row
	 * @param columnCount the number of the column
	 * @param value       the content
	 */
	private void createCell(Row row, int columnCount, Object value) {
		sheet.autoSizeColumn(columnCount);
		Cell cell = row.createCell(columnCount);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else {
			cell.setCellValue((String) value);
		}
	}

	/**
	 * create the xslx file
	 */
	private void createXlsx() {
		sheet = workbook.createSheet(SHEETNAME);
		Row row = sheet.createRow(0);
		createCell(row, 0, CODE_VALUE);
		createCell(row, 1, CODE_LABEL);
		createCell(row, 2, CODE_CLASS);
		int rowCount = 1;
		for (MissingBean bean : list) {
			row = sheet.createRow(rowCount++);
			int colCount = 0;
			createCell(row, colCount++, bean.getCodeValue());
			createCell(row, colCount++, bean.getCodeLabel());
			createCell(row, colCount++, bean.getCodeClass());
		}
	}

	/**
	 * write the xlsx file to the response stream
	 * 
	 * @param response the response
	 * @throws IOException on io errors
	 */
	public void export(HttpServletResponse response) throws IOException {
		createXlsx();

		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();

		outputStream.close();
	}

	private Integer getColumnId(String expected, String... found) {
		for (int i = 0; i < found.length; i++) {
			if (expected.equalsIgnoreCase(found[i])) {
				return i;
			}
		}
		return null;
	}

	private String ensureString(Cell cell) throws IOException {
		if (cell == null) {
			return null;
		} else if (CellType.STRING.equals(cell.getCellType())) {
			return cell.getStringCellValue();
		} else if (CellType.NUMERIC.equals(cell.getCellType())) {
			Double d = cell.getNumericCellValue();
			return d == null ? null : String.valueOf(d.intValue());
		} else {
			throw new IOException("unsupported cell type " + cell.getCellType());
		}
	}
	
	private void ensureNotNull(Integer notNullValue, String message, String appendix) throws IOException {
		if (notNullValue == null) {
			StringBuilder buf = new StringBuilder();
			buf.append(message).append(" ").append(appendix);
			throw new IOException(buf.toString());
		}
	}

	/**
	 * convert input stream of xlsx file to list of missing beans
	 * 
	 * @param stream the stream
	 * @return the list of missing beans; may be an empty list
	 * @throws IOException on io errors
	 */
	public List<MissingBean> fromXlsx(InputStream stream) throws IOException {
		XSSFWorkbook wb = new XSSFWorkbook(stream);
		XSSFSheet sheet = wb.getSheet(SHEETNAME);
		if (sheet == null) {
			sheet = wb.getSheetAt(0);
		}
		List<MissingBean> list = new ArrayList<>();
		if (sheet != null) {
			Integer sheetNumber = 0;
			Integer codeValueCol = null;
			Integer codeLabelCol = null;
			Integer codeClassCol = null;
			for (Row row : sheet) {
				sheetNumber++;
				if (sheetNumber > 1) {
					ensureNotNull(codeValueCol, "missing column with name", CODE_VALUE);
					ensureNotNull(codeLabelCol, "missing column with name", CODE_LABEL);
					ensureNotNull(codeClassCol, "missing column with name", CODE_CLASS);
					String codeValue = ensureString(row.getCell(codeValueCol));
					String codeLabel = ensureString(row.getCell(codeLabelCol));
					String codeClass = ensureString(row.getCell(codeClassCol));
					MissingBean bean = new MissingBean();
					bean.setCodeValue(codeValue);
					bean.setCodeLabel(codeLabel);
					bean.setCodeClass(codeClass);
					list.add(bean);
				} else {
					String cell0Value = ensureString(row.getCell(0));
					String cell1Value = ensureString(row.getCell(1));
					String cell2Value = ensureString(row.getCell(2));
					codeValueCol = getColumnId(CODE_VALUE, cell0Value, cell1Value, cell2Value);
					codeLabelCol = getColumnId(CODE_LABEL, cell0Value, cell1Value, cell2Value);
					codeClassCol = getColumnId(CODE_CLASS, cell0Value, cell1Value, cell2Value);
				}
			}
			wb.close();
		} else {
			wb.close();
			throw new IOException("no sheet missinglist found in xlsx file, aborting");
		}
		return list;
	}
}