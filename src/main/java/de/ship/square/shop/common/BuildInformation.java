package de.ship.square.shop.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Component;

/**
 * 
 * @author henkej
 *
 */
@Component
public class BuildInformation {

	@Value("${server.servlet.context-path:}")
	private String contextPath;

	@Autowired(required = false)
	private BuildProperties buildProperties;

	public String getVersion() {
		return buildProperties != null ? buildProperties.getVersion() : "0.0.0";
	}

	public String getContextPath() {
		return (contextPath != null && !contextPath.isBlank()) ? String.format("/%s/", contextPath) : "/";
	}
}
