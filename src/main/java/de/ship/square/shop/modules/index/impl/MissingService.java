package de.ship.square.shop.modules.index.impl;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import de.ship.dbppsquare.square.enums.EnumIsocode;
import de.ship.square.shop.common.xlsx.ExcelConverter;
import de.ship.square.shop.modules.index.IMissingService;
import de.ship.square.shop.modules.index.impl.missing.MissingBean;
import de.ship.square.shop.modules.index.impl.missing.MissingGateway;
import de.ship.square.shop.modules.index.impl.missing.MissinglistBean;
import de.ship.square2.tools.model.KeyValueBean;

/**
 * 
 * @author henkej
 *
 */
@Service
public class MissingService implements IMissingService {

	@Autowired
	private MissingGateway gateway;
	
	@Override
	public List<MissinglistBean> getAllLists(String username, EnumIsocode isocode) {
		return gateway.getAllLists(username, isocode);
	}

	@Override
	public List<KeyValueBean> getAllStudygroups(String username, EnumIsocode isocode) {
		return gateway.getAllStudies(username, isocode);
	}

	@Override
	public Boolean upsertMissinglist(MissinglistBean bean) {
		return gateway.upsertMissinglist(bean) == 1;
	}

	@Override
	public MissinglistBean getMissinglist(Integer id) {
		return gateway.getMissinglist(id);
	}

	@Override
	public Boolean deleteMissinglist(Integer id) {
		return gateway.deleteMissinglist(id) > 0;
	}

	@Override
	public void download(List<MissingBean> missingBeans, HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment; filename=missinglist.xlsx");
		ExcelConverter generator = new ExcelConverter(missingBeans);
		generator.export(response);
	}

	@Override
	public void upload(MultipartFile file, Integer missinglistId, Boolean replace) throws IOException {
		ExcelConverter converter = new ExcelConverter(null);
		List<MissingBean> missings = converter.fromXlsx(file.getInputStream());
		gateway.upsertMissings(missings, missinglistId, replace);
	}
}
