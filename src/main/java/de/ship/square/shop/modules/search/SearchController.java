package de.ship.square.shop.modules.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import de.ship.square.shop.common.SessionBean;
import de.ship.square.shop.modules.ControllerTemplate;
import de.ship.square.shop.modules.ISessionService;
import de.ship.square.shop.modules.index.impl.IndexService;
import de.ship.square.shop.modules.index.impl.jstree.DetailsBean;
import de.ship.square.shop.modules.menu.MenuitemBeanConverter;

/**
 * 
 * @author henkej
 *
 */
@Controller
public class SearchController extends ControllerTemplate {
	private final static org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(SearchController.class);

	@Autowired
	private ISessionService sessionService;

	@Autowired
	private MenuitemBeanConverter menuitemBeanConverter;
	
	@Autowired
	private IndexService indexService;

	@Autowired
	private SessionBean sessionBean;

	@Value("${reference.application.menu}")
	private String menuurl;

	@GetMapping("/search")
	public String loadSearchPage(HttpServletRequest request, Model model) {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		setupUserSession(request, indexService, sessionBean);
		model.addAttribute("needle", null);
		model.addAttribute("results", new ArrayList<DetailsBean>());
		return "search";
	}
	
	@PostMapping("/search")
	public String find(@ModelAttribute SearchBean bean, HttpServletRequest request, Model model) throws ServletException {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		setupUserSession(request, indexService, sessionBean);
		String userName = indexService.getUsername(request);
		LOGGER.debug("search for ", bean.getNeedle());
		Set<String> found = indexService.findAsSet(bean.getNeedle());
		found.add(bean.getNeedle()); // if needle contains a unique_name, findAsSet won't find it
		List<DetailsBean> list = new ArrayList<>();
		for (String uniqueName : found) {
			LOGGER.debug("found {}", uniqueName);
			DetailsBean b = indexService.getDetails(userName, uniqueName);
			if (b.getKeys().size() > 0) { // if needle is not a unique_name, its details bean is meaningless
				b.set("unique_name", uniqueName);
				list.add(b);
			}
		}
		model.addAttribute("needle", bean.getNeedle());
		model.addAttribute("results", list);
		return "search";
	}

}
