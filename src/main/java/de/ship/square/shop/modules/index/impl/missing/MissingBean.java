package de.ship.square.shop.modules.index.impl.missing;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class MissingBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String codeValue;
	private String codeLabel;
	private String codeClass;

	/**
	 * @return the codeValue
	 */
	public String getCodeValue() {
		return codeValue;
	}

	/**
	 * @param codeValue the codeValue to set
	 */
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}

	/**
	 * @return the codeLabel
	 */
	public String getCodeLabel() {
		return codeLabel;
	}

	/**
	 * @param codeLabel the codeLabel to set
	 */
	public void setCodeLabel(String codeLabel) {
		this.codeLabel = codeLabel;
	}

	/**
	 * @return the codeClass
	 */
	public String getCodeClass() {
		return codeClass;
	}

	/**
	 * @param codeClass the codeClass to set
	 */
	public void setCodeClass(String codeClass) {
		this.codeClass = codeClass;
	}
}
