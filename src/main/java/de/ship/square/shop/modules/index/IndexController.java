package de.ship.square.shop.modules.index;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;

import org.apache.logging.log4j.LogManager;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import de.ship.dbppsquare.square.enums.EnumIsocode;
import de.ship.square.shop.common.SessionBean;
import de.ship.square.shop.modules.ControllerTemplate;
import de.ship.square.shop.modules.ISessionService;
import de.ship.square.shop.modules.index.impl.IndexService;
import de.ship.square.shop.modules.index.impl.missing.MissingBean;
import de.ship.square.shop.modules.index.impl.missing.MissinglistBean;
import de.ship.square.shop.modules.menu.MenuitemBeanConverter;

/**
 * 
 * @author henkej
 *
 */
@Controller
// @RolesAllowed({"read", "write"})
public class IndexController extends ControllerTemplate {
	private final static org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(IndexController.class);

	@Autowired
	private ISessionService sessionService;

	@Autowired
	private MenuitemBeanConverter menuitemBeanConverter;

	@Autowired
	private IndexService indexService;

	@Autowired
	private IMissingService missingService;

	@Autowired
	private SessionBean sessionBean;

	@Value("${reference.application.menu}")
	private String menuurl;

	@GetMapping("/")
	public String homePage(Model model, HttpServletRequest request) {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		super.setupUserSession(request, indexService, sessionBean);
		return "index";
	}

	/**
	 * for the ajax calls - returns the json string for jsTree
	 * 
	 * @param uniqueName the unique name of the element for loading its children
	 * @param decendants if true, load all decendants of the children
	 * @param parents    if true, load all parents of uniqueName, too
	 * @param request    the http request
	 * @param model      the model
	 * @return the json string of the array of found elements; an empty json array
	 *         at least
	 * @throws ServletException on servlet errors
	 */
	@GetMapping("/load/{uniqueName}")
	public ResponseEntity<String> login(@PathVariable String uniqueName, @PathParam(value = "false") Boolean decendants,
			@PathParam(value = "false") Boolean parents, HttpServletRequest request, Model model) throws ServletException {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		super.setupUserSession(request, indexService, sessionBean);
		return ResponseEntity
				.ok(indexService.getElements(sessionBean.getUsername(), uniqueName, false, decendants, parents));
	}

	@GetMapping("/find/{needle}")
	public ResponseEntity<String> find(@PathVariable String needle, HttpServletRequest request, Model model) {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		return ResponseEntity.ok(indexService.find(needle));
	}

	/**
	 * for the ajax calls - return the json string of details
	 * 
	 * @param uniqueName the unique name of the element
	 * @param request    the http request
	 * @param model      the model
	 * @return the json string of the element; an empty object at least
	 * @throws ServletException on servlet errors
	 */
	@GetMapping("/details/{uniqueName}")
	public ResponseEntity<String> details(@PathVariable String uniqueName, HttpServletRequest request, Model model)
			throws ServletException {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		super.setupUserSession(request, indexService, sessionBean);
		return ResponseEntity.ok(indexService.getDetails(sessionBean.getUsername(), uniqueName).toJson());
	}

	@GetMapping("/missinglist/add")
	public String addMissinglist(HttpServletRequest request, Model model) throws ServletException {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		super.setupUserSession(request, indexService, sessionBean);

		// TODO: get from current locale
		EnumIsocode isocode = EnumIsocode.de_DE;

		model.addAttribute("missinglistBean", new MissinglistBean());
		model.addAttribute("studygroups", missingService.getAllStudygroups(sessionBean.getUsername(), isocode));
		return "missinglist/edit";
	}

	@GetMapping("/missinglist/{id}")
	public String editMissinglist(HttpServletRequest request, Model model, @PathVariable Integer id) {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		super.setupUserSession(request, indexService, sessionBean);

		// TODO: get from current locale
		EnumIsocode isocode = EnumIsocode.de_DE;

		model.addAttribute("missinglistBean", missingService.getMissinglist(id));
		model.addAttribute("studygroups", missingService.getAllStudygroups(sessionBean.getUsername(), isocode));

		return "missinglist/edit";
	}

	@GetMapping("/missinglist/delete/{id}")
	public String deleteMissinglist(HttpServletRequest request, Model model, @PathVariable Integer id)
			throws ServletException {
		missingService.deleteMissinglist(id);
		return login(request, model);
	}

	@PostMapping("/missinglist/upsert")
	public String upsertMissinglist(HttpServletRequest request, Model model, @ModelAttribute MissinglistBean bean)
			throws ServletException {
		Boolean result = missingService.upsertMissinglist(bean);
		return result ? login(request, model) : addMissinglist(request, model);
	}

	@PostMapping("/missinglist/upload")
	public String uploadMissinglist(@RequestParam("file") MultipartFile file, @RequestParam("id") Integer id,
			@RequestParam(value = "replace", required = false, defaultValue = "false") Boolean replace, HttpServletRequest request, Model model)
			throws IOException {
		missingService.upload(file, id, replace);
		return editMissinglist(request, model, id);
	}

	@GetMapping("/missinglist/download/{id}")
	public void downloadMissinglist(HttpServletResponse response, @PathVariable Integer id) throws IOException {
		List<MissingBean> missingBeans = missingService.getMissinglist(id).getMissings();
		missingService.download(missingBeans, response);
	}

	@GetMapping("/login")
	public String login(HttpServletRequest request, Model model) throws ServletException {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		super.setupUserSession(request, indexService, sessionBean);

		// TODO: get from current locale
		EnumIsocode isocode = EnumIsocode.de_DE;

		model.addAttribute("elements", indexService.getElements(sessionBean.getUsername(), null, true, false, false));
		model.addAttribute("missinglists", missingService.getAllLists(sessionBean.getUsername(), isocode));
		return "welcome";
	}

	@GetMapping("/logout")
	public String logout(HttpServletRequest request) throws ServletException {
		request.logout();
		return "redirect:/";
	}

	@RequestMapping("/logging")
	public String changeLogLevel(HttpServletRequest request, Model model, @RequestParam String level) {
		Logger logger = (Logger) LoggerFactory.getILoggerFactory().getLogger("root");
		if (logger != null) {
			logger.setLevel(Level.toLevel(level));
			LOGGER.info(String.format("Changed root logger to level %s", level));
			setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
			return "logging";
		} else {
			LOGGER.error(String.format("root logger not found, cannot set it to level %s", level));
			return "error";
		}
	}
}
