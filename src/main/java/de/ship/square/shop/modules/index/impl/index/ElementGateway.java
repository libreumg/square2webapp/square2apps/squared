package de.ship.square.shop.modules.index.impl.index;

import static de.ship.dbppsquare.square.Tables.T_CONFOUNDER;
import static de.ship.dbppsquare.square.Tables.T_ELEMENT;
import static de.ship.dbppsquare.square.Tables.T_GROUPPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_METADATATYPE;
import static de.ship.dbppsquare.square.Tables.T_MISSINGLIST;
import static de.ship.dbppsquare.square.Tables.T_PERSON;
import static de.ship.dbppsquare.square.Tables.T_SCALE;
import static de.ship.dbppsquare.square.Tables.T_TRANSLATION;
import static de.ship.dbppsquare.square.Tables.T_USERGROUP;
import static de.ship.dbppsquare.square.Tables.T_USERPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_VARIABLE;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEATTRIBUTE;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEGROUP;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEGROUPELEMENT;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEUSAGE;
import static de.ship.dbppsquare.square.Tables.V_HAYSTACK;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.JSONB;
import org.jooq.Record1;
import org.jooq.Record15;
import org.jooq.Record4;
import org.jooq.SelectConditionStep;
import org.jooq.SelectHavingStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.ship.dbppsquare.square.enums.EnumControlvar;
import de.ship.dbppsquare.square.enums.EnumDatatype;
import de.ship.dbppsquare.square.enums.EnumIsocode;
import de.ship.dbppsquare.square.tables.TElement;
import de.ship.square.shop.modules.index.impl.jstree.DetailsBean;
import de.ship.square.shop.modules.index.impl.jstree.NodeBean;
import de.ship.square2.tools.db.SquareGateway;

/**
 * 
 * @author henkej
 *
 */
@Repository
public class ElementGateway extends SquareGateway {
	private final static Logger LOGGER = LogManager.getLogger(ElementGateway.class);

	private final static Integer EXPECTED_VERSION = 76;

	private final DSLContext jooq;

	public ElementGateway(@Autowired DSLContext jooq) {
		this.jooq = jooq;
		Integer version = super.getVersion(jooq);
		if (version < EXPECTED_VERSION) {
			throw new DataAccessException(
					String.format("Database version %d is too slow, need at least %d", version, EXPECTED_VERSION));
		}
	}

	/**
	 * get all children of the element referenced by parentId
	 * 
	 * @param username      the name of the user for privilege checks
	 * @param parentId      the ID of the parent element in the hierarchy tree
	 * @param nodeIcon      the css classes for the node icon
	 * @param leafIcon      the css classes for the leaf icon
	 * @param isocode       the isocode for the translation (current language)
	 * @param includeParent if true, add the parent to the resulting set, if false,
	 *                      don't
	 * @param decendants    if true, load all decendants of the children
	 * @return the list of found node beans; an empty one at least
	 */
	public List<NodeBean> getChildren(String username, String parentId, String nodeIcon, String leafIcon,
			EnumIsocode isocode, Boolean includeParent, Boolean decendants) {
		TElement PARENT = T_ELEMENT.as("parent");
		TElement CHILD = T_ELEMENT.as("child");
		Field<Integer> AMOUNT = DSL.field("amount", Integer.class);
		SelectHavingStep<Record4<String, String, String, Integer>> sql = jooq
		// @formatter:off
			.select(T_ELEMENT.UNIQUE_NAME, T_TRANSLATION.VALUE, PARENT.UNIQUE_NAME, DSL.count(CHILD.PK).as(AMOUNT))
			.from(T_ELEMENT)
			.leftJoin(T_TRANSLATION).on(T_TRANSLATION.FK_LANG.eq(T_ELEMENT.FK_LANG)).and(T_TRANSLATION.ISOCODE.eq(isocode))
			.leftJoin(PARENT).on(PARENT.PK.eq(T_ELEMENT.FK_PARENT))
			.leftJoin(CHILD).on(CHILD.FK_PARENT.eq(T_ELEMENT.PK))
			.where(includeParent ? T_ELEMENT.UNIQUE_NAME.eq(parentId) : DSL.falseCondition())
			.or(parentId == null ? T_ELEMENT.FK_PARENT.isNull() : PARENT.UNIQUE_NAME.eq(parentId))
			.groupBy(T_ELEMENT.UNIQUE_NAME, T_TRANSLATION.VALUE, PARENT.UNIQUE_NAME);
		// @formatter:on
		LOGGER.debug(sql.toString());
		List<NodeBean> list = new ArrayList<>();
		for (Record4<String, String, String, Integer> r : sql.fetch()) {
			String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
			String translation = r.get(T_TRANSLATION.VALUE);
			String parentUniqueName = r.get(PARENT.UNIQUE_NAME);
			Integer amountChildren = r.get(AMOUNT);
			StringBuilder buf = new StringBuilder();
			buf.append(translation = translation == null ? uniqueName : translation);
			if (amountChildren > 0) {
				buf.append(" (").append(amountChildren).append(")");
			}
			NodeBean bean = new NodeBean(uniqueName, parentUniqueName, buf.toString(),
					amountChildren > 0 ? nodeIcon : leafIcon);
			list.add(bean);
		}
		if (decendants) {
			list = getDecendants(list, isocode, nodeIcon, leafIcon);
		}
		return list;
	}

	/**
	 * get all decendants (recursively) of the nodes in list
	 * 
	 * @param list     the list of parent nodes
	 * @param isocode  the isocode of the translation
	 * @param nodeIcon the css classes for the node icon
	 * @param leafIcon the css classes for the leaf icon
	 * @return the merged list of all nodes
	 */
	private List<NodeBean> getDecendants(List<NodeBean> list, EnumIsocode isocode, String nodeIcon, String leafIcon) {
		TElement PARENT = T_ELEMENT.as("parent");
		TElement CHILD = T_ELEMENT.as("child");
		Field<Integer> AMOUNT = DSL.field("amount", Integer.class);
		Field<String> PARENT_UNIQUE_NAME = DSL.field("parent_unique_name", String.class);
		String X = "x";
		Field<Integer> X_k = DSL.field("k", Integer.class);
		Field<String> X_u = DSL.field("u", String.class);
		Field<String> X_t = DSL.field("t", String.class);
		Field<String> X_p = DSL.field("p", String.class);

		Set<String> parents = new HashSet<String>();
		for (NodeBean bean : list) {
			parents.add(bean.getId());
		}

		SelectHavingStep<Record4<String, String, String, Integer>> sql = jooq
		// @formatter:off
			.withRecursive(X, X_k.getName(), X_u.getName(), X_t.getName(), X_p.getName()).as(jooq
			  .select(T_ELEMENT.PK, T_ELEMENT.UNIQUE_NAME, T_TRANSLATION.VALUE, PARENT.UNIQUE_NAME)
			  .from(T_ELEMENT)
			  .leftJoin(T_TRANSLATION).on(T_TRANSLATION.FK_LANG.eq(T_ELEMENT.FK_LANG)).and(T_TRANSLATION.ISOCODE.eq(isocode))
			  .leftJoin(PARENT).on(PARENT.PK.eq(T_ELEMENT.FK_PARENT))
			  .where(PARENT.UNIQUE_NAME.in(parents))
			  .unionAll(jooq
		  		.select(T_ELEMENT.PK, T_ELEMENT.UNIQUE_NAME, T_TRANSLATION.VALUE, PARENT.UNIQUE_NAME)
				  .from(T_ELEMENT)
				  .leftJoin(T_TRANSLATION).on(T_TRANSLATION.FK_LANG.eq(T_ELEMENT.FK_LANG)).and(T_TRANSLATION.ISOCODE.eq(isocode))
				  .leftJoin(PARENT).on(PARENT.PK.eq(T_ELEMENT.FK_PARENT))
				  .innerJoin(X).on(X_u.eq(PARENT.UNIQUE_NAME))))
			.select(X_u.as(T_ELEMENT.UNIQUE_NAME), X_t.as(T_TRANSLATION.VALUE), X_p.as(PARENT_UNIQUE_NAME), DSL.count(CHILD.PK).as(AMOUNT))
			.from(X)
			.leftJoin(CHILD).on(CHILD.FK_PARENT.eq(X_k))
			.groupBy(X_u, X_t, X_p);
		// @formatter:on
		LOGGER.debug(sql.toString());
		for (Record4<String, String, String, Integer> r : sql.fetch()) {
			String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
			String translation = r.get(T_TRANSLATION.VALUE);
			String parent = r.get(PARENT_UNIQUE_NAME);
			Integer amount = r.get(AMOUNT);
			NodeBean bean = new NodeBean(uniqueName, parent, translation, amount > 0 ? nodeIcon : leafIcon);
			list.add(bean);
		}
		return list;
	}

	/**
	 * get all parents of uniqueName
	 * 
	 * @param username   the name of the user for privilege checks
	 * @param uniqueName the ID of the parent element in the hierarchy tree
	 * @param nodeIcon   the css classes for the node icon
	 * @param isocode    the isocode for the translation (current language)
	 * @return the list of found node beans; an empty one at least
	 */
	public List<NodeBean> getParents(String username, String uniqueName, String nodeIcon, EnumIsocode isocode) {
		TElement PARENT = T_ELEMENT.as("parent");
		TElement CHILD = T_ELEMENT.as("child");
		Field<Integer> AMOUNT = DSL.field("amount", Integer.class);
		Field<String> PARENT_UNIQUE_NAME = DSL.field("parent_unique_name", String.class);
		String X = "x";
		Field<Integer> X_k = DSL.field("k", Integer.class);
		Field<String> X_u = DSL.field("u", String.class);
		Field<String> X_t = DSL.field("t", String.class);
		Field<String> X_p = DSL.field("p", String.class);
		Field<Integer> X_a = DSL.field("a", Integer.class); // ancestor

		List<NodeBean> list = new ArrayList<>();

		SelectHavingStep<Record4<String, String, String, Integer>> sql = jooq
		// @formatter:off
			.withRecursive(X, X_k.getName(), X_u.getName(), X_t.getName(), X_p.getName(), X_a.getName()).as(jooq
			  .select(T_ELEMENT.PK, T_ELEMENT.UNIQUE_NAME, T_TRANSLATION.VALUE, PARENT.UNIQUE_NAME, T_ELEMENT.FK_PARENT)
			  .from(T_ELEMENT)
			  .leftJoin(T_TRANSLATION).on(T_TRANSLATION.FK_LANG.eq(T_ELEMENT.FK_LANG)).and(T_TRANSLATION.ISOCODE.eq(isocode))
			  .leftJoin(PARENT).on(PARENT.PK.eq(T_ELEMENT.FK_PARENT))
			  .where(T_ELEMENT.UNIQUE_NAME.eq(uniqueName))
			  .unionAll(jooq
		  		.select(T_ELEMENT.PK, T_ELEMENT.UNIQUE_NAME, T_TRANSLATION.VALUE, PARENT.UNIQUE_NAME, T_ELEMENT.FK_PARENT)
				  .from(T_ELEMENT)
				  .innerJoin(X).on(X_a.eq(T_ELEMENT.PK))
				  .leftJoin(T_TRANSLATION).on(T_TRANSLATION.FK_LANG.eq(T_ELEMENT.FK_LANG)).and(T_TRANSLATION.ISOCODE.eq(isocode))
				  .leftJoin(PARENT).on(PARENT.PK.eq(T_ELEMENT.FK_PARENT))
				  ))
			.select(X_u.as(T_ELEMENT.UNIQUE_NAME), X_t.as(T_TRANSLATION.VALUE), X_p.as(PARENT_UNIQUE_NAME), DSL.count(CHILD.PK).as(AMOUNT))
			.from(X)
			.leftJoin(CHILD).on(CHILD.FK_PARENT.eq(X_k))
			.groupBy(X_u, X_t, X_p);
		// @formatter:on
		LOGGER.debug(sql.toString());
		for (Record4<String, String, String, Integer> r : sql.fetch()) {
			String thatUniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
			String translation = r.get(T_TRANSLATION.VALUE);
			String parent = r.get(PARENT_UNIQUE_NAME);
			Integer amount = r.get(AMOUNT);
			StringBuilder buf = new StringBuilder();
			buf.append(translation = translation == null ? uniqueName : translation);
			if (amount > 0) {
				buf.append(" (").append(amount).append(")");
			}
			NodeBean bean = new NodeBean(thatUniqueName, parent, buf.toString(), nodeIcon);
			LOGGER.debug("found %s", bean);
			list.add(bean);
		}

		return list;
	}

	/**
	 * get the details of uniqueName
	 * 
	 * @param username   the name of the user
	 * @param uniqueName the unique name of the element
	 * @return the details bean; an empty bean at least
	 */
	public DetailsBean getDetails(String username, String uniqueName, EnumIsocode isocode) {
		SelectConditionStep<Record15<Integer, JSONB, String, String, EnumControlvar, EnumDatatype, Boolean, String, Integer, String, String, String, String, String, String>> sql = jooq
		// @formatter:off
			.selectDistinct(T_ELEMENT.ORDER_NR, T_ELEMENT.TRANSLATION, T_MISSINGLIST.NAME, T_VARIABLE.COLUMN_NAME, T_VARIABLE.CONTROL_TYPE, T_VARIABLE.DATATYPE, T_VARIABLE.IDVARIABLE, 
					            T_VARIABLE.VALUELIST, T_VARIABLE.VAR_ORDER, T_SCALE.NAME, T_VARIABLEUSAGE.NAME, T_VARIABLEGROUP.NAME, T_METADATATYPE.NAME, 
					            T_VARIABLEATTRIBUTE.NAME, T_VARIABLEATTRIBUTE.VALUE)
			.from(T_ELEMENT)
			.leftJoin(T_GROUPPRIVILEGE).on(T_GROUPPRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
			.leftJoin(T_USERGROUP).on(T_USERGROUP.FK_GROUP.eq(T_GROUPPRIVILEGE.FK_GROUP))
			.leftJoin(T_USERPRIVILEGE).on(T_USERPRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
			.leftJoin(T_PERSON).on(T_PERSON.USNR.eq(T_USERGROUP.FK_USNR).or(T_PERSON.USNR.eq(T_USERPRIVILEGE.FK_USNR)))        
			.leftJoin(T_MISSINGLIST).on(T_MISSINGLIST.PK.eq(T_ELEMENT.FK_MISSINGLIST))
			.leftJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(T_ELEMENT.PK))
			.leftJoin(T_VARIABLEUSAGE).on(T_VARIABLEUSAGE.PK.eq(T_VARIABLE.FK_VARIABLEUSAGE))
			.leftJoin(T_VARIABLEATTRIBUTE).on(T_VARIABLEATTRIBUTE.FK_VARIABLE.eq(T_VARIABLE.FK_ELEMENT))
			.leftJoin(T_SCALE).on(T_SCALE.PK.eq(T_VARIABLE.FK_SCALE))
			.leftJoin(T_VARIABLEGROUPELEMENT).on(T_VARIABLEGROUPELEMENT.FK_ELEMENT.eq(T_ELEMENT.PK))
			.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP))
			.leftJoin(T_CONFOUNDER).on(T_CONFOUNDER.FK_REFERENCED_ELEMENT.eq(T_ELEMENT.PK))
			.leftJoin(T_METADATATYPE).on(T_METADATATYPE.PK.eq(T_CONFOUNDER.FK_METADATATYPE))
			.where(T_ELEMENT.UNIQUE_NAME.eq(uniqueName))
			.and(T_PERSON.USERNAME.eq(username));
		// @formatter:on
		LOGGER.debug(sql.toString());
		DetailsBean bean = DetailsBean.instance(uniqueName);
		for (Record15<Integer, JSONB, String, String, EnumControlvar, EnumDatatype, Boolean, String, Integer, String, String, String, String, String, String> r : sql
				.fetch()) {
			bean.set("order_nr", r.get(T_ELEMENT.ORDER_NR));
			bean.set("translation", r.get(T_ELEMENT.TRANSLATION));
			bean.set("missinglist", r.get(T_MISSINGLIST.NAME));
			bean.set("column_name", r.get(T_VARIABLE.COLUMN_NAME));
			bean.set("control_type", r.get(T_VARIABLE.CONTROL_TYPE));
			bean.set("datatype", r.get(T_VARIABLE.DATATYPE));
			bean.set("id_variable", r.get(T_VARIABLE.IDVARIABLE));
			bean.set("valuelist", r.get(T_VARIABLE.VALUELIST));
			bean.set("var_order", r.get(T_VARIABLE.VAR_ORDER));
			bean.set("scale", r.get(T_SCALE.NAME));
			bean.set("variableusage", r.get(T_VARIABLEUSAGE.NAME));
			bean.set("variablegroup", r.get(T_VARIABLEGROUP.NAME));
			bean.set("is_confounder_as", r.get(T_METADATATYPE.NAME));
			bean.set(r.get(T_VARIABLEATTRIBUTE.NAME), r.get(T_VARIABLEATTRIBUTE.VALUE));
		}
		return bean;
	}

	/**
	 * search for needle in V_HAYSTACK
	 * 
	 * @param needle the search phrase
	 * @return a set of found unique_names; an empty set at least
	 */
	public Set<String> search(String needle) {
		String searchString = String.format("%%%s%%", needle);
		SelectConditionStep<Record1<String>> sql = jooq
		// @formatter:off
			.select(V_HAYSTACK.UNIQUE_NAME)
			.from(V_HAYSTACK)
			.where(V_HAYSTACK.NEEDLE.like(searchString));
		// @formatter:on
		LOGGER.debug(sql.toString());
		Set<String> set = new HashSet<>();
		for (Record1<String> r : sql.fetch()) {
			set.add(r.get(V_HAYSTACK.UNIQUE_NAME));
		}
		return set;
	}
}
