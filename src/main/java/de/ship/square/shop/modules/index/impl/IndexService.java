package de.ship.square.shop.modules.index.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.GsonBuilder;

import de.ship.dbppsquare.square.enums.EnumIsocode;
import de.ship.square.shop.modules.ISessionService;
import de.ship.square.shop.modules.index.IIndexService;
import de.ship.square.shop.modules.index.impl.index.ElementGateway;
import de.ship.square.shop.modules.index.impl.jstree.DetailsBean;
import de.ship.square.shop.modules.index.impl.jstree.NodeBean;

/**
 * 
 * @author henkej
 *
 */
@Service
public class IndexService implements ISessionService, IIndexService {

	@Autowired
	private ElementGateway elementGateway;

	@Override
	public String getElements(String username, String parentId, Boolean addParent, Boolean decendants, Boolean parents) {
		final String nodeIcon = "fa-regular fa-folder";
		final String leafIcon = "fa-solid fa-file";

		// TODO: get from current locale
		EnumIsocode isocode = EnumIsocode.de_DE;
		
		List<NodeBean> list = elementGateway.getChildren(username, parentId, nodeIcon, leafIcon, isocode, addParent, decendants);
		list.addAll(elementGateway.getParents(username, parentId, nodeIcon, isocode));
		
		return new GsonBuilder().create().toJson(list);
	}

	@Override
	public DetailsBean getDetails(String username, String uniqueName) {
		// TODO: get from current locale
		EnumIsocode isocode = EnumIsocode.de_DE;
		
		DetailsBean bean = elementGateway.getDetails(username, uniqueName, isocode);
		return bean;
	}

	@Override
	public Set<String> findAsSet(String needle) {
		return elementGateway.search(needle);
	}
	
	@Override
	public String find(String needle) {
		return new GsonBuilder().create().toJson(findAsSet(needle));
	}
}
