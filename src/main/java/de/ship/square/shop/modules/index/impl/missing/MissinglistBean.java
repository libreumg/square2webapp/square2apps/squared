package de.ship.square.shop.modules.index.impl.missing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class MissinglistBean implements Serializable {
	private static final long serialVersionUID = 4L;

	private Integer pk;
	private String name;
	private String description;
	private Integer studygroup;
	private String studygroupName;
	private List<MissingBean> missings;

	public MissinglistBean() {
		this.missings = new ArrayList<>();
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the studygroup
	 */
	public Integer getStudygroup() {
		return studygroup;
	}

	/**
	 * @param studygroup the studygroup to set
	 */
	public void setStudygroup(Integer studygroup) {
		this.studygroup = studygroup;
	}

	/**
	 * @return the studygroupName
	 */
	public String getStudygroupName() {
		return studygroupName;
	}

	/**
	 * @param studygroupName the studygroupName to set
	 */
	public void setStudygroupName(String studygroupName) {
		this.studygroupName = studygroupName;
	}

	/**
	 * @return the missings
	 */
	public List<MissingBean> getMissings() {
		return missings;
	}
}
