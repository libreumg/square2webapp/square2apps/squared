package de.ship.square.shop.modules.search;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class SearchBean implements Serializable {
	private static final long serialVersionUID = 1;
	private String needle;

	/**
	 * @return the needle
	 */
	public String getNeedle() {
		return needle;
	}

	/**
	 * @param needle the needle to set
	 */
	public void setNeedle(String needle) {
		this.needle = needle;
	}
}
