package de.ship.square.shop.modules.index.impl.jstree;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.jooq.JSONB;

import com.google.gson.GsonBuilder;

/**
 * 
 * @author henkej
 *
 */
public class DetailsBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private final String id;
	private final Map<String, Object> map;

	private DetailsBean(String id, Map<String, Object> map) {
		this.id = id;
		this.map = map;
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id == null ? "null" : id.replace(".", "-");
	}

	/**
	 * initiate with an empty map
	 * 
	 * @param id the id
	 * 
	 * @return the bean
	 */
	public static final DetailsBean instance(String id) {
		return new DetailsBean(id, new HashMap<>());
	}

	/**
	 * initiate with the given map; if null, use an empty map instead
	 * 
	 * @param id the id
	 * @param map the map
	 * @return the bean
	 */
	public static final DetailsBean instance(String id, Map<String, Object> map) {
		return new DetailsBean(id, map == null ? new HashMap<>() : map);
	}

	/**
	 * set the element of the map
	 * 
	 * @param key the key
	 * @param value the value
	 */
	public void set(String key, Object value) {
		Object found = map.get(key);
		if (value != null && value instanceof JSONB) {
			value = ((JSONB) value).data();
		}
		if (found != null) {
			Set<Object> values = new HashSet<>();
			if (found instanceof Set<?>) {
				values.addAll((Set<?>) found);
			} else {
			  values.add(map.get(key));
			}
			values.add(value);
			map.put(key, values);
		} else {
			map.put(key, value);
		}
	}
	
	/**
	 * get the element of the map
	 * 
	 * @param key the key
	 * @return the value or null if not found
	 */
	public Object get(String key) {
		return map.get(key);
	}
	
	/**
	 * get all keys of the internal map
	 * 
	 * @return all the keys that are set here
	 */
	public Set<String> getKeys() {
		return map.keySet();
	}
	
	/**
	 * @return the map
	 */
	public Map<String, Object> getMap() {
		return map;
	}

	/**
	 * convert the content into a json object; do not add empty fields
	 * 
	 * @return the json object; an empty one at least
	 */
	public String toJson() {		
		return new GsonBuilder().create().toJson(map);
	}
}
