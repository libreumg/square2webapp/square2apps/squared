package de.ship.square.shop.modules.index;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import de.ship.dbppsquare.square.enums.EnumIsocode;
import de.ship.square.shop.modules.index.impl.missing.MissingBean;
import de.ship.square.shop.modules.index.impl.missing.MissinglistBean;
import de.ship.square2.tools.model.KeyValueBean;

/**
 * 
 * @author henkej
 *
 */
public interface IMissingService {

	/**
	 * get all missing lists that this user has access to
	 * 
	 * @param username the name of the current user
	 * @param isocode  the isocode for the current translations
	 * @return a list of missinglist beans; an empty one at least
	 */
	public List<MissinglistBean> getAllLists(String username, EnumIsocode isocode);

	/**
	 * get a list of all studies that the user has access to
	 * 
	 * @param username the name of the current user
	 * @param isocode  the isocode for the current translation
	 * @return a list of studies; an empty list at least
	 */
	public List<KeyValueBean> getAllStudygroups(String username, EnumIsocode isocode);

	/**
	 * upsert the missinglist
	 * 
	 * @param bean the missinglist bean
	 * @return true or false
	 */
	public Boolean upsertMissinglist(MissinglistBean bean);

	/**
	 * get the missinglist with the id
	 * 
	 * @param id the id of the missinglist
	 * @return the bean or null
	 */
	public MissinglistBean getMissinglist(Integer id);

	/**
	 * delete the missinglist
	 * 
	 * @param id the id of the missing list
	 * @return true or false
	 */
	public Boolean deleteMissinglist(Integer id);

	/**
	 * download missing list
	 * 
	 * @param missingBeans the missings
	 * @param response     the response
	 * @throws IOException on io errors
	 */
	public void download(List<MissingBean> missingBeans, HttpServletResponse response) throws IOException;

	/**
	 * upload missing list
	 * 
	 * @param file          the xlsx file content
	 * @param missinglistId the id of the missinglist
	 * @param replace       if true, replace all old values by this list; if false,
	 *                      merge the lists
	 * @throws IOException on io errors
	 */
	public void upload(MultipartFile file, Integer missinglistId, Boolean replace) throws IOException;

}
