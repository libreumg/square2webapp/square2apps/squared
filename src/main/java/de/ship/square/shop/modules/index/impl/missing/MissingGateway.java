package de.ship.square.shop.modules.index.impl.missing;

import static de.ship.dbppsquare.square.Tables.T_ELEMENT;
import static de.ship.dbppsquare.square.Tables.T_GROUPPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_MISSING;
import static de.ship.dbppsquare.square.Tables.T_MISSINGLIST;
import static de.ship.dbppsquare.square.Tables.T_MISSINGTYPE;
import static de.ship.dbppsquare.square.Tables.T_PERSON;
import static de.ship.dbppsquare.square.Tables.T_STUDYGROUP;
import static de.ship.dbppsquare.square.Tables.T_USERGROUP;
import static de.ship.dbppsquare.square.Tables.T_USERPRIVILEGE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DMLQuery;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertOnDuplicateSetMoreStep;
import org.jooq.Record2;
import org.jooq.Record5;
import org.jooq.SelectConditionStep;
import org.jooq.SelectWhereStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.ship.dbppsquare.square.enums.EnumIsocode;
import de.ship.dbppsquare.square.tables.records.TMissingRecord;
import de.ship.dbppsquare.square.tables.records.TMissinglistRecord;
import de.ship.dbppsquare.square.tables.records.TMissingtypeRecord;
import de.ship.square2.tools.LambdaResultWrapper;
import de.ship.square2.tools.db.SquareGateway;
import de.ship.square2.tools.model.KeyValueBean;

/**
 * 
 * @author henkej
 *
 */
@Repository
public class MissingGateway extends SquareGateway {
	private final static Logger LOGGER = LogManager.getLogger(MissingGateway.class);

	private final static Integer EXPECTED_VERSION = 76;

	private final DSLContext jooq;

	public MissingGateway(@Autowired DSLContext jooq) {
		this.jooq = jooq;
		Integer version = super.getVersion(jooq);
		if (version < EXPECTED_VERSION) {
			throw new DataAccessException(
					String.format("Database version %d is too slow, need at least %d", version, EXPECTED_VERSION));
		}
	}

	/**
	 * get all missing lists from the database where the user as access to
	 * 
	 * @param username the name of the user
	 * @param isocode  the isocode, currently ignored
	 * @return a list of missinglist beans; an empty list at least
	 */
	public List<MissinglistBean> getAllLists(String username, EnumIsocode isocode) {
		SelectConditionStep<Record5<Integer, String, String, Integer, String>> sql = jooq
		// @formatter:off
			.selectDistinct(T_MISSINGLIST.PK,
											T_MISSINGLIST.NAME,
					    				T_MISSINGLIST.DESCRIPTION,
					    				T_MISSINGLIST.FK_ELEMENT,
					    				T_ELEMENT.NAME)
			.from(T_MISSINGLIST)
			.leftJoin(T_STUDYGROUP).on(T_STUDYGROUP.FK_ELEMENT.eq(T_MISSINGLIST.FK_ELEMENT))
			.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_MISSINGLIST.FK_ELEMENT))
			.leftJoin(T_USERPRIVILEGE).on(T_USERPRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
			.leftJoin(T_GROUPPRIVILEGE).on(T_GROUPPRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
			.leftJoin(T_USERGROUP).on(T_USERGROUP.FK_GROUP.eq(T_GROUPPRIVILEGE.FK_GROUP))
			.leftJoin(T_PERSON).on(T_PERSON.USNR.in(T_USERPRIVILEGE.FK_USNR, T_USERGROUP.FK_USNR))
			.where(T_PERSON.USERNAME.eq(username));
		// @formatter:on
		LOGGER.debug(sql.toString());
		List<MissinglistBean> list = new ArrayList<>();
		for (Record5<Integer, String, String, Integer, String> r : sql.fetch()) {
			MissinglistBean bean = new MissinglistBean();
			bean.setPk(r.get(T_MISSINGLIST.PK));
			bean.setName(r.get(T_MISSINGLIST.NAME));
			bean.setDescription(r.get(T_MISSINGLIST.DESCRIPTION));
			bean.setStudygroup(r.get(T_MISSINGLIST.FK_ELEMENT));
			bean.setStudygroupName(r.get(T_ELEMENT.NAME));
			list.add(bean);
		}
		return list;
	}

	/**
	 * get all studies from the database
	 * 
	 * @param username the name of the user
	 * @param isocode  the isocode, currently ignored
	 * @return the list of found studies
	 */
	public List<KeyValueBean> getAllStudies(String username, EnumIsocode isocode) {
		SelectConditionStep<Record2<Integer, String>> sql = jooq
		// @formatter:off
			.selectDistinct(T_STUDYGROUP.FK_ELEMENT, T_ELEMENT.NAME)
			.from(T_STUDYGROUP)
			.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_STUDYGROUP.FK_ELEMENT))
			.leftJoin(T_USERPRIVILEGE).on(T_USERPRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
			.leftJoin(T_GROUPPRIVILEGE).on(T_GROUPPRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
			.leftJoin(T_USERGROUP).on(T_USERGROUP.FK_GROUP.eq(T_GROUPPRIVILEGE.FK_GROUP))
			.leftJoin(T_PERSON).on(T_PERSON.USNR.in(T_USERPRIVILEGE.FK_USNR, T_USERGROUP.FK_USNR))
			.where(T_PERSON.USERNAME.eq(username));
		// @formatter:on
		LOGGER.debug(sql.toString());
		List<KeyValueBean> list = new ArrayList<>();
		for (Record2<Integer, String> r : sql.fetch()) {
			KeyValueBean bean = new KeyValueBean();
			bean.setKey(r.get(T_STUDYGROUP.FK_ELEMENT));
			bean.setValue(r.get(T_ELEMENT.NAME));
			list.add(bean);
		}
		return list;
	}

	/**
	 * upsert the bean
	 * 
	 * @param bean the bean
	 * @return number of affected database rows; should be 1
	 */
	public Integer upsertMissinglist(MissinglistBean bean) {
		DMLQuery<TMissinglistRecord> sql = null;
		if (bean.getPk() == null) {
			sql = jooq
			// @formatter:off
				.insertInto(T_MISSINGLIST,
										T_MISSINGLIST.NAME,
										T_MISSINGLIST.DESCRIPTION,
										T_MISSINGLIST.FK_ELEMENT)
				.values(bean.getName(), bean.getDescription(), bean.getStudygroup())
				.onConflict(T_MISSINGLIST.NAME)
				.doUpdate()
				.set(T_MISSINGLIST.DESCRIPTION, bean.getDescription())
				.set(T_MISSINGLIST.FK_ELEMENT, bean.getStudygroup());
			// @formatter:on
		} else {
			sql = jooq
			// @formatter:off
				.update(T_MISSINGLIST)
				.set(T_MISSINGLIST.NAME, bean.getName())
				.set(T_MISSINGLIST.DESCRIPTION, bean.getDescription())
				.set(T_MISSINGLIST.FK_ELEMENT, bean.getStudygroup())
				.where(T_MISSINGLIST.PK.eq(bean.getPk()));
			// @formatter:on
		}
		LOGGER.debug(sql.toString());
		return sql.execute();
	}

	/**
	 * get the missinglist with the id and its elements
	 * 
	 * @param id the id of the missinglist
	 * @return the bean or null
	 */
	public MissinglistBean getMissinglist(Integer id) {
		SelectWhereStep<TMissingtypeRecord> sql0 = jooq.selectFrom(T_MISSINGTYPE);
		LOGGER.debug(sql0.toString());
		Map<Integer, String> typeMap = new HashMap<>();
		for (TMissingtypeRecord r : sql0.fetch()) {
			typeMap.put(r.getPk(), r.getName());
		}
		SelectConditionStep<TMissinglistRecord> sql1 = jooq.selectFrom(T_MISSINGLIST).where(T_MISSINGLIST.PK.eq(id));
		LOGGER.debug(sql1.toString());
		for (TMissinglistRecord r : sql1.fetch()) {
			MissinglistBean bean = new MissinglistBean();
			bean.setPk(r.getPk());
			bean.setName(r.getName());
			bean.setDescription(r.getDescription());
			bean.setStudygroup(r.getFkElement());
			SelectConditionStep<TMissingRecord> sql2 = jooq.selectFrom(T_MISSING).where(T_MISSING.FK_MISSINGLIST.eq(id));
			LOGGER.debug(sql2.toString());
			for (TMissingRecord rm : sql2.fetch()) {
				MissingBean mBean = new MissingBean();
				mBean.setCodeClass(typeMap.get(rm.getFkMissingtype()));
				mBean.setCodeLabel(rm.getNameInStudy());
				mBean.setCodeValue(rm.getCode());
				bean.getMissings().add(mBean);
			}
			return bean;
		}
		return null;
	}

	/**
	 * delete the missinglist and all of its missings
	 * 
	 * @param id the id of the missinglist
	 * @return number of affected database rows
	 */
	public Integer deleteMissinglist(Integer id) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		jooq.transaction(t -> {
			DeleteConditionStep<TMissingRecord> sql0 = DSL.using(t).deleteFrom(T_MISSING)
					.where(T_MISSING.FK_MISSINGLIST.eq(id));
			LOGGER.debug(sql0.toString());
			lrw.add(sql0.execute());
			DeleteConditionStep<TMissinglistRecord> sql1 = DSL.using(t).deleteFrom(T_MISSINGLIST)
					.where(T_MISSINGLIST.PK.eq(id));
			LOGGER.debug(sql1.toString());
			lrw.add(sql1.execute());
		});
		return lrw.getInteger();
	}

	/**
	 * upsert the missings of the list
	 * 
	 * @param missings      all the missings
	 * @param missinglistId the ID of the corresponding missing list
	 * @param replace       if true, delete all values from the missing list before
	 *                      importing it
	 * @return the number of affected database rows
	 */
	public Integer upsertMissings(List<MissingBean> missings, Integer missinglistId, Boolean replace) {
		SelectWhereStep<TMissingtypeRecord> sql0 = jooq.selectFrom(T_MISSINGTYPE);
		LOGGER.debug(sql0.toString());
		Map<String, Integer> map = new HashMap<>();
		for (TMissingtypeRecord r : sql0.fetch()) {
			String name = r.getName();
			if (name != null) {
				map.put(name.toUpperCase(), r.getPk());
			}
		}
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		jooq.transaction(t -> {
			if (replace) {
				DeleteConditionStep<TMissingRecord> sql1 = DSL.using(t).deleteFrom(T_MISSING)
						.where(T_MISSING.FK_MISSINGLIST.eq(missinglistId));
				LOGGER.debug(sql1.toString());
				lrw.add(sql1.execute());
			}
			for (MissingBean bean : missings) {
				String codeClass = bean.getCodeClass();
				Integer missingtype = map.get(codeClass != null ? codeClass.toUpperCase() : null);
				if (missingtype == null) { // undefined missing types are missings
					missingtype = map.get("MISSING");
				}
				if (missingtype == null) {
					throw new DataAccessException(
							"T_MISSINGTYPE does not contain an entity called MISSING (ignore case), aborting...");
				}
				InsertOnDuplicateSetMoreStep<TMissingRecord> sql2 = DSL.using(t)
				// @formatter:off
					.insertInto(T_MISSING,
											T_MISSING.CODE,
											T_MISSING.NAME_IN_STUDY, 
											T_MISSING.FK_MISSINGTYPE, 
											T_MISSING.FK_MISSINGLIST)
					.values(bean.getCodeValue(), bean.getCodeLabel(), missingtype, missinglistId)
					.onConflict(T_MISSING.FK_MISSINGLIST, T_MISSING.CODE)
					.doUpdate()
					.set(T_MISSING.NAME_IN_STUDY, bean.getCodeLabel())
					.set(T_MISSING.FK_MISSINGTYPE, missingtype);
				// @formatter:on
				LOGGER.debug(sql2.toString());
				lrw.add(sql2.execute());
			}
		});
		return lrw.getInteger();
	}

}
