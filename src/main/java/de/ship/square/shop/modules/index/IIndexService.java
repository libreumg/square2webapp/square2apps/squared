package de.ship.square.shop.modules.index;

import java.util.Set;

import de.ship.square.shop.modules.index.impl.jstree.DetailsBean;

/**
 * 
 * @author henkej
 *
 */
public interface IIndexService {
	/**
	 * get all elements in the hierarchy tree directly under the element of parentId
	 * 
	 * @param username the name of the user for privilege checks
	 * @param parentId the ID of the parent element
	 * @param addParent if true, add parent to result
	 * @param decendants if true, load full stack of decendants of the children
	 * @param parents if true, load all parents of parentId, too
	 * @return the json representation of a list of jsTree elements
	 */
	public String getElements(String username, String parentId, Boolean addParent, Boolean decendants, Boolean parents);
	
	/**
	 * get details on the element referenced by uniqueName
	 * 
	 * @param username the name of the user for privilege checks
	 * @param uniqueName the id of the element
	 * @return the details bean
	 */
	public DetailsBean getDetails(String username, String uniqueName);
	
	/**
	 * search for needle in the database
	 * 
	 * @param needle the search text
	 * @return a json list of unique names
	 */
	public String find(String needle);

	/**
	 * search for needle in the database
	 * 
	 * @param needle the search text
	 * @return a set of found unique names; an empty set at least
	 */
	public Set<String> findAsSet(String needle);
}
