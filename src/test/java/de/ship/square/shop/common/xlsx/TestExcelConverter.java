package de.ship.square.shop.common.xlsx;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.ship.square.shop.modules.index.impl.missing.MissingBean;

/**
 * 
 * @author henkej
 *
 */
public class TestExcelConverter {

	private void testFile(String filename) throws IOException {
		File file = new File(filename);
		assertNotNull(file);
		assertTrue(file.exists());
		InputStream is = new FileInputStream(file);
		ExcelConverter converter = new ExcelConverter(null);
		List<MissingBean> list = converter.fromXlsx(is);
		assertNotNull(list);
		assertEquals(1, list.size());
		MissingBean bean = list.get(0);
		assertNotNull(bean);
		assertEquals("missing", bean.getCodeClass());
		assertEquals("99999", bean.getCodeValue());
		assertEquals("refused", bean.getCodeLabel());
	}
	
	/**
	 * test if the xlsx import routine works
	 * 
	 * @throws IOException on io errors
	 */
	@Test
	public void testFromXlsx() throws IOException {
		testFile("src/test/resources/testcase1.xlsx");
		testFile("src/test/resources/testcase2.xlsx");
		testFile("src/test/resources/testcase3.xlsx");
		testFile("src/test/resources/testcase4.xlsx");
	}
}
