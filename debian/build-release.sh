#!/bin/bash

mkdir -p debian/squared/var/lib/squared

chmod 755 -R debian/squared/DEBIAN
chmod 644 debian/squared/DEBIAN/control

./gradlew clean build

G=$(grep "^version =" build.gradle | sed -e "s/version = //g" | sed -e "s/'//g")

if [ -f "build/libs/SQuaReD-$G.jar" ]
then
  echo "created file build/libs/SQuaReD-$G.jar"
else
  >&2 echo "could not find file build/libs/SQuaReD-$G.jar"
  exit 1
fi

echo "found version $G"

rm -f debian/squared/var/lib/squared/*.jar
cp -v build/libs/SQuaReD-$G.jar debian/squared/var/lib/squared

sed -i debian/squared/DEBIAN/control -e "s/VERSION/$G/g"
sed -i debian/squared/usr/bin/squared -e "s/VERSION/$G/g"

V=$(grep "Version" debian/squared/DEBIAN/control | sed -e "s/Version: //g")

fakeroot dpkg-deb -b -Zxz debian/squared debian/squared_$V.deb
